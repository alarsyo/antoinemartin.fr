{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    futils.url = "github:numtide/flake-utils";
  };

  outputs = {
    self,
    nixpkgs,
    futils,
  } @ inputs: let
    inherit (nixpkgs) lib;
    inherit (futils.lib) eachDefaultSystem defaultSystems;
  in (eachDefaultSystem (
    system: let
      pkgs = import nixpkgs {inherit system;};
    in {
      packages = {
        default = self.packages.${system}.website;
        website = pkgs.stdenvNoCC.mkDerivation {
          name = "antoinemartin.fr-website";

          src = self;

          nativeBuildInputs = with pkgs; [
            hugo
          ];

          buildPhase = ''
            hugo --minify
          '';

          installPhase = ''
            cp -r public $out
          '';

          meta = with pkgs.lib; {
            platforms = platforms.all;
          };
        };
      };

      devShell = pkgs.mkShellNoCC {
        buildInputs = with pkgs; [
          hugo
        ];
      };
    }
  ));
}
